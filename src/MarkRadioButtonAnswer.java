import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

@TestMethodOrder(OrderAnnotation.class)
class MarkRadioButtonAnswer extends setupTest{

	public boolean isAlertPresent() 
	{ 
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }  
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }   
	}   
	
	//TC05
	@Test
	@Order(1)
	public void markRadioButtonsAnswersQuestion1() {
		firstRun();
		setupBuildTest();
		setupFields();
//		On Trivia logo screen, press Start.
		startButton.click();
		
//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());

//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Make sure that 4 fields are filled with the answers typed.
//		Mark the radio button of the first answer.
//		Make sure that radio button for: �ANSWER ONE QUESTION ONE� is now marked.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, mark the radio button of the second answer.
//		Make sure that radio button for: �ANSWER TWO QUESTION ONE� is now marked.
		selectRadioButton(1);
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, mark the radio button of the third answer.
//		Make sure that radio button for: �ANSWER THREE QUESTION ONE� is now marked.
		selectRadioButton(2);
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, mark the radio button of the fourth answer.
//		Make sure that radiselectRadioButton(1);o button for: �ANSWER FOUR QUESTION ONE� is now marked.
		selectRadioButton(3);
		
//		Close web browser window.
//		Make sure that web browser window is closed.
		removeFields();
		nextTest();
	}
	
	//TC07
	@Test
	@Order(2)
	public void markRadioButtonAnswer1AnswerCorrect() {
		setupBuildTest();
		setupFields();
//		On Trivia logo screen, press Start.
		startButton.click();
		
//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		
//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Make sure that 4 fields are filled with the answers typed.
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, mark the radio button of the first answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		nextButtonBuildTest.click();
		
//		Make sure that �Please type here your question : question number: 2� is shown.
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		
//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO�, and press on Next.
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Make sure that 4 fields are filled with the answers typed.
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, mark the radio button of the first answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 0);
		nextButtonBuildTest.click();
		
//		Make sure that �Please type here your question : question number: 3� is shown.
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		
//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE�, and press on Next.
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Make sure that 4 fields are filled with the answers typed.
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 3� screen, mark the radio button of the first answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 0);
		nextButtonBuildTest.click();
		
//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		
//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the first answer and press Next!
		answersQoneRadioButtonsTest.get(0).click();
		nextButtonTest.click();
		
//		Make sure that the second question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQtwoRadioButtonsTest.get(0).click();
		nextButtonTest.click();
		
//		Make sure that the third question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQthreeRadioButtonsTest.get(0).click();
		nextButtonTest.click();
		
//		Make sure that Test summary is shown, and green �Success� banner is shown.
		assertEquals("Sucsses", testResult.getText());
		
//		On Test summary screen, press the Quit button
//		Make sure that Trivia web application is closed � white page is shown in browser.
		QuitButtonTest.click();
		
//		Close web browser window
//		Make sure that web browser window is closed.
		removeFields();
		driver.get("https://shemsvcollege.github.io/Trivia");
	}
	
	//TC08
	@Test
	@Order(3)
	public void markRadioButtonAnswer1AnswerWrong() {
		setupBuildTest();
		setupFields();
		boolean failed = false;
		
//		On Trivia logo screen, press Start.
		startButton.click();

//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());

//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		EnterQuestion("TEST QUESTION ONE"); 
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		mark the radio button of the first answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		nextButtonBuildTest.click();
		
//		Make sure that �Please type here your question : question number: 2� is shown.
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());

//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO�, and press on Next.
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		mark the radio button of the first answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 0);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 3� is shown.
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());

//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE�, and press on Next.
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 3� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		mark the radio button of the first answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 0);
		nextButtonBuildTest.click();

//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		assertEquals("You finished to build the test - lets play!!", readyToTest.getText());

//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the first answer and press Next!
		answersQoneRadioButtonsTest.get(0).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQtwoRadioButtonsTest.get(0).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the second answer and press Next!
		answersQthreeRadioButtonsTest.get(1).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		try {
			assertEquals("Failed", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
			

//		On Test summary screen, press the Try Again button
		tryAgainButtonTest.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the first answer and press Next!
		answersQoneRadioButtonsTest.get(0).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the third answer and press Next!
		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQthreeRadioButtonsTest.get(0).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		try {
			assertEquals("Failed", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		
//		On Test summary screen, press the Try Again button
		tryAgainButtonTest.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQoneRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQtwoRadioButtonsTest.get(0).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQthreeRadioButtonsTest.get(0).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		try {
			assertEquals("Failed", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		
//		On Test summary screen, press the Quit button
		QuitButtonTest.click();

//		Make sure that Trivia web application is closed � white page is shown in browser.
//		Close web browser window
//		Make sure that web browser window is closed.
		removeFields();
		nextTest();
		
		if(failed) {
			fail("Test has failed. See messages in console.");
		}
		
	}
	
	//TC09
	@Disabled
	@Test
	@Order(4)
	public void markRadioButtonAnswer2AnswerCorrect() {
		setupBuildTest();
		setupFields();
		
//		On Trivia logo screen, press Start.
		startButton.click();
		
//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		
//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Mark the radio button of the second answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 1);
		nextButtonBuildTest.click();
		
//		Make sure that �Please type here your question : question number: 2� is shown.
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		
//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO�, and press on Next.
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Mark the radio button of the second answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 1);
		nextButtonBuildTest.click();
		
//		Make sure that �Please type here your question : question number: 3� is shown.
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		
//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE�, and press on Next.
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Mark the radio button of the second answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 1);
		nextButtonBuildTest.click();
		
//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		
//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the second answer and press Next!
		answersQoneRadioButtonsTest.get(1).click();
		nextButtonTest.click();
		
//		Make sure that the second question in Test is shown.
//		On Test screen, choose the second answer and press Next!
		answersQtwoRadioButtonsTest.get(1).click();
		nextButtonTest.click();
		
//		Make sure that the third question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQthreeRadioButtonsTest.get(1).click();
		nextButtonTest.click();
		
//		Make sure that Test summary is shown, and green �Success� banner is shown.
		assertEquals("Sucsses", testResult.getText());
		
//		On Test summary screen, press the Quit button
//		Make sure that Trivia web application is closed � white page is shown in browser.
		QuitButtonTest.click();
		
//		Close web browser window
//		Make sure that web browser window is closed.
		removeFields();
		driver.get("https://shemsvcollege.github.io/Trivia");
	}
	
	//TC10	
	@Disabled
	@Test
	@Order(5)
	public void markRadioButtonAnswer2AnswerWrong() {
		setupBuildTest();
		setupFields();
		boolean failed = false;
		
//		On Trivia logo screen, press Start.
		startButton.click();
		
//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());

//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		mark the radio button of the second answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 1);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 2� is shown.
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());

//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO�, and press on Next.
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Mark the radio button of the second answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 1);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 3� is shown.
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());

//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE�, and press on Next.
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Mark the radio button of the second answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 1);
		nextButtonBuildTest.click();

//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		assertEquals("You finished to build the test - lets play!!", readyToTest.getText());

//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the second answer and press Next!
		answersQoneRadioButtonsTest.get(1).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the second answer and press Next!
		answersQtwoRadioButtonsTest.get(1).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQthreeRadioButtonsTest.get(0).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		try {
			assertEquals("Failed", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		
		

//		On Test summary screen, press the Try Again button
		tryAgainButtonTest.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the second answer and press Next!
		answersQoneRadioButtonsTest.get(1).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the third answer and press Next!
		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the second answer and press Next!
		answersQthreeRadioButtonsTest.get(1).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		assertEquals("Failed", testResult.getText());

//		On Test summary screen, press the Try Again button
		tryAgainButtonTest.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQoneRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the second answer and press Next!
		answersQtwoRadioButtonsTest.get(1).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the second answer and press Next!
		answersQthreeRadioButtonsTest.get(1).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		assertEquals("Failed", testResult.getText());

//		On Test summary screen, press the Quit button
		QuitButtonTest.click();

//		Make sure that Trivia web application is closed � white page is shown in browser.
//		Close web browser window
//		Make sure that web browser window is closed.
		driver.get("https://shemsvcollege.github.io/Trivia");
		nextTest();
		if(failed) {
			fail("Test has failed. See messages in console.");
		}
	}

	//TC11
	@Disabled
	@Test
	@Order(6)
	public void markRadioButtonAnswer3AnswerCorrect() {
		setupBuildTest();
		setupFields();

//		On Trivia logo screen, press Start.
		startButton.click();

//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());

//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 2� is shown.
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());

//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO�, and press on Next.
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();


//		Make sure that �Please type here your question : question number: 3� is shown.
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());

//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE�, and press on Next.
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		assertEquals("You finished to build the test - lets play!!", readyToTest.getText());

//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the third answer and press Next!
		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the third answer and press Next!
		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the third answer and press Next!
		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and green �Success� banner is shown.
		assertEquals("Sucsses", testResult.getText());

//		On Test summary screen, press the Quit button
//		Make sure that Trivia web application is closed � white page is shown in browser.
		QuitButtonTest.click();

//		Close web browser window
//		Make sure that web browser window is closed.
		removeFields();
		driver.get("https://shemsvcollege.github.io/Trivia");
	}

	//TC12
	@Disabled
	@Test
	@Order(7)
	public void markRadioButtonAnswer3AnswerWrong() {
		setupBuildTest();
		setupFields();
		boolean failed = false;

//		On Trivia logo screen, press Start.
		startButton.click();

//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());

//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 2� is shown.
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());

//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO�, and press on Next.
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 3� is shown.
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());

//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE�, and press on Next.
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		assertEquals("You finished to build the test - lets play!!", readyToTest.getText());

//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the third answer and press Next!
		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the third answer and press Next!
		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQthreeRadioButtonsTest.get(0).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		try {
			assertEquals("Failed", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

//		On Test summary screen, press the Try Again button
		tryAgainButtonTest.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the third answer and press Next!
		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the second answer and press Next!
		answersQtwoRadioButtonsTest.get(1).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the third answer and press Next!
		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		assertEquals("Failed", testResult.getText());

//		On Test summary screen, press the Try Again button
		tryAgainButtonTest.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQoneRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the third answer and press Next!
		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the third answer and press Next!
		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		assertEquals("Failed", testResult.getText());

//		On Test summary screen, press the Quit button
		QuitButtonTest.click();

//		Make sure that Trivia web application is closed � white page is shown in browser.
//		Close web browser window
//		Make sure that web browser window is closed.
		driver.get("https://shemsvcollege.github.io/Trivia");
		nextTest();
		if(failed) {
			fail("Test has failed. See messages in console.");
		}

	}

	//TC13
	@Disabled
	@Test
	@Order(8)
	public void markRadioButtonAnswer4AnswerCorrect() {
		setupBuildTest();
		setupFields();

//		On Trivia logo screen, press Start.
		startButton.click();

//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());

//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Mark the radio button of the fourth answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 3);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 2� is shown.
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());

//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO�, and press on Next.
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Mark the radio button of the fourth answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 3);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 3� is shown.
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());

//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE�, and press on Next.
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Mark the radio button of the fourth answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 3);
		nextButtonBuildTest.click();

//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		assertEquals("You finished to build the test - lets play!!", readyToTest.getText());

//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQoneRadioButtonsTest.get(3).click();
		nextButtonTest.click();
	
//		Make sure that the second question in Test is shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQtwoRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQthreeRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and green �Success� banner is shown.
		assertEquals("Success", testResult.getText());

//		On Test summary screen, press the Quit button
//		Make sure that Trivia web application is closed � white page is shown in browser.
		QuitButtonTest.click();

//		Close web browser window
//		Make sure that web browser window is closed.
		removeFields();
		driver.get("https://shemsvcollege.github.io/Trivia");
	}

	//TC14
	@Disabled
	@Test
	@Order(9)
	public void markRadioButtonAnswer4AnswerWrong() {
		setupBuildTest();
		setupFields();
		boolean failed = false;
		
//		On Trivia logo screen, press Start.
		startButton.click();

//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		
//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Mark the radio button of the fourth answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 3);
		nextButtonBuildTest.click();


//		Make sure that �Please type here your question : question number: 2� is shown.
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());

//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO�, and press on Next.
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Mark the radio button of the fourth answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 3);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 3� is shown.
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());

//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE�, and press on Next.
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Mark the radio button of the fourth answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 3);
		nextButtonBuildTest.click();

//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		assertEquals("You finished to build the test - lets play!!", readyToTest.getText());

//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQoneRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQtwoRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the first answer and press Next!
		answersQthreeRadioButtonsTest.get(0).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		assertEquals("Failed", testResult.getText());

//		On Test summary screen, press the Try Again button
		tryAgainButtonTest.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQoneRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the second answer and press Next!
		answersQtwoRadioButtonsTest.get(1).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQthreeRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		try {
			assertEquals("Failed", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

//		On Test summary screen, press the Try Again button
		tryAgainButtonTest.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
//		On Test screen, choose the third answer and press Next!
		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();

//		Make sure that the second question in Test is shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQtwoRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that the third question in Test is shown.
//		On Test screen, choose the fourth answer and press Next!
		answersQthreeRadioButtonsTest.get(3).click();
		nextButtonTest.click();

//		Make sure that Test summary is shown, and red �Failed� banner is shown.
		assertEquals("Failed", testResult.getText());

//		On Test summary screen, press the Quit button
		QuitButtonTest.click();

//		Make sure that Trivia web application is closed � white page is shown in browser.
//		Close web browser window
//		Make sure that web browser window is closed.
		driver.get("https://shemsvcollege.github.io/Trivia");
		nextTest();
		if(failed) {
			fail("Test has failed. See messages in console.");
		}
	}

	//TC15
	@Disabled
	@Test
	@Order(10)
	public void markRadioButtonAnswerDontAnswerTest() {
		setupBuildTest();
		setupFields();
		Alert alertMessage = null;
		String Qone = null;
		String Qtwo = null;
		String Qthree = null;

//		On Trivia logo screen, press Start.
		startButton.click();

//		Make sure that �Please type here your question : question number: 1� is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());

//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE�, and press on Next.
		Qone = "TEST QUESTION ONE";
		EnterQuestion(Qone);
		nextButtonBuildTest.click();
		

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Mark the radio button of the first answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 2� is shown.
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());

//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO�, and press on Next.
		Qtwo = "TEST QUESTION TWO";
		EnterQuestion(Qtwo);
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Mark the radio button of the second answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 1);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 3� is shown.
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());

//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE�, and press on Next.
		Qthree = "TEST QUESTION THREE";
		EnterQuestion(Qthree);
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		assertEquals("You finished to build the test - lets play!!", readyToTest.getText());

//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that Test screen is now shown.
		assertEquals(Qthree, QoneTitle.getText());
		
//		On Test screen, press Next!
		nextButtonTest.click();
		
//		Make sure that error message: �You have to mark an answer!!� is shown.� is shown.
		
		if(isAlertPresent()) {
			alertMessage = driver.switchTo().alert();
			assertEquals("You have to mark an answer!!", alertMessage.getText());
		}
		else {
			assertEquals(Qthree, QoneTitle.getText());
		}
		
//		Press �OK� on error message
		alertMessage.accept();
		alertMessage = null;
			
//		Make sure that �Test� screen is still shown.
		assertEquals(Qthree, QoneTitle.getText());
		
//		On Test screen, choose the first answer and press Next!
		answersQoneRadioButtonsTest.get(0).click();
		nextButtonTest.click();
		
//		Make sure that the second question in Test is shown.
		assertEquals(Qtwo, QtwoTitle.getText());
		
//		On Test screen, press Next!
		nextButtonTest.click();
		
//		Make sure that error message: �You have to mark an answer!!� is shown.� is shown. XXXX
		if(isAlertPresent()) {
			alertMessage = driver.switchTo().alert();
			assertEquals("You have to mark an answer!!", alertMessage.getText());
		}
		else {
			assertEquals(Qtwo, QtwoTitle.getText());
		}
//		Press �OK� on error message
		alertMessage.accept();
		alertMessage = null;
		
//		Make sure that �Test� screen is still shown.
		assertEquals(Qtwo, QtwoTitle.getText());
		
//		On Test screen, choose the second answer and press Next!
		answersQtwoRadioButtonsTest.get(1).click();
		nextButtonTest.click();
		
//		Make sure that the third question in Test is shown.
		assertEquals(Qone, QthreeTitle.getText());
		
//		On Test screen, press Next!
		nextButtonTest.click();
		
//		Make sure that error message: �You have to mark an answer!!� is shown.� is shown.
		if(isAlertPresent()) {
			alertMessage = driver.switchTo().alert();
			assertEquals("You have to mark an answer!!", alertMessage.getText());
		}
		
		else {
			assertEquals(Qone, QthreeTitle.getText());
		}
		
//		Press �OK� on error message
		alertMessage.accept();
		alertMessage = null;
		
//		Make sure that �Test� screen is still shown.
		assertEquals(Qone, QthreeTitle.getText());
		
//		Close web browser window
//		Make sure that web browser window is closed.
		finalize();

	}
	
}
