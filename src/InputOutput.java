import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

class InputOutput extends setupTest{
	//TC21
	@Test
	@Order(1)
	void checkSingleQuestionMark () {
		firstRun();
		setupBuildTest();
		setupFields();

		boolean failed = false;

		startButton.click();
		
		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}


		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

			EnterQuestion("TEST QUESTION TWO");
			nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		playButton.click();
		setupTestElements();

		try {
			assertEquals("?", QoneTitle.getText().substring(QoneTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("?", QtwoTitle.getText().substring(QtwoTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("?", QthreeTitle.getText().substring(QthreeTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}


		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("Sucsses", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		QuitButtonTest.click();
		removeFields();
		driver.get("https://shemsvcollege.github.io/Trivia");
		setupBuildTest();
		setupFields();

		startButton.click();
		
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());

		EnterQuestion("TEST QUESTION ONE?"); 
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterQuestion("TEST QUESTION TWO?");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterQuestion("TEST QUESTION THREE?");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		playButton.click();
		setupTestElements();

		try {
			assertEquals("?", QoneTitle.getText().substring(QoneTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("?", QtwoTitle.getText().substring(QtwoTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("?", QthreeTitle.getText().substring(QthreeTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("Sucsses", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		QuitButtonTest.click();
		removeFields();
		
		finalize();
		if(failed) {
			fail("Test has failed. See messages in console.");
		}
	
/*
	@Test
	@Order(1)
	void CheckSingleQuestionMark () {
		firstRun();
		setupBuildTest();
		setupFields();

		boolean failed = false;
		
//		On Trivia logo screen, press Start.
		startButton.click();

//		Make sure that �Please type here your question : question number: 1� is shown.
		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
	
//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE� � without question mark, and press on Next.
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 2� is shown.
		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}

//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO� � without question mark, and press on Next.
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();

//		Make sure that �Please type here your question : question number: 3� is shown.
		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}

//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE� � without question mark, and press on Next.
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}

//		On �Please enter 4 possible answers and Mark the right one! ; question number: 3� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		try {
			assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}
		
//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();

//		Make sure that question  has only one question mark at the end of the title.
		try {
			assertTrue("Missing question mark at the end of question no.1 title", QoneTitle.getText().contains("?"));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On Test screen, choose the third answer and press Next!
		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();
		
//		Make sure that question has only one question mark at the end of the title.
		try {
			assertTrue("Missing question mark at the end of question no.2 title", QtwoTitle.getText().contains("?"));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On Test screen, choose the third answer and press Next!
		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();
		
//		Make sure that question  has only one question mark at the end of the title.
		try {
			assertTrue("Missing question mark at the end of question no.3 title", QthreeTitle.getText().contains("?"));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On Test screen, choose the third answer and press Next!
		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();
		
//		Make sure that Test summary is shown, and green �Success� banner is shown.
		try {
			assertEquals("Sucsses", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On Test summary screen, press the Quit button
		QuitButtonTest.click();
		removeFields();
		
//		Make sure that Trivia web application is closed � white page is shown in browser.
//		Go to address: https://shemsvcollege.github.io/Trivia/
//		Make sure that Trivia logo screen is now shown.
		driver.get("https://shemsvcollege.github.io/Trivia");
		setupBuildTest();
		setupFields();
		
//		On Trivia logo screen, press Start.
		startButton.click();
		
//		Make sure that �Please type here your question : question number: 1� is shown.
		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE� � with single question mark, and press on Next.
		EnterQuestion("TEST QUESTION ONE?");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();
		
//		Make sure that �Please type here your question : question number: 2� is shown.
		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO� � with single question mark, and press on Next.
		EnterQuestion("TEST QUESTION TWO?");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();
		
//		Make sure that �Please type here your question : question number: 3� is shown.
		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE� � with single question mark, and press on Next.
		EnterQuestion("TEST QUESTION THREE?");
		nextButtonBuildTest.click();
		
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Mark the radio button of the third answer, and press on Next.
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();
		
//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
		try {
			assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On �You finished to build the test � let�s play!!� screen, press the Play button.
		playButton.click();
		setupTestElements();
		
//		Make sure that question  has only one question mark at the end of the title.
		try {
			assertTrue("Missing question mark at the end of question no.1 title", QoneTitle.getText().contains("?"));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On Test screen, choose the third answer and press Next!
		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();
		
//		Make sure that question has only one question mark at the end of the title.
		try {
			assertTrue("Missing question mark at the end of question no.2 title", QtwoTitle.getText().contains("?"));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On Test screen, choose the third answer and press Next!
		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();
		
//		Make sure that question  has only one question mark at the end of the title.
		try {
			assertTrue("Missing question mark at the end of question no.3 title", QthreeTitle.getText().contains("?"));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On Test screen, choose the third answer and press Next!
		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();
		
//		Make sure that Test summary is shown, and green �Success� banner is shown.
		try {
			assertEquals("Sucsses", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			failed = true;
		}
		
//		On Test summary screen, press the Quit button
//		Make sure that Trivia web application is closed � white page is shown in browser.
//		Go to address: https://shemsvcollege.github.io/Trivia/
//		Make sure that Trivia logo screen is now shown.
//		On Trivia logo screen, press Start.
//		Make sure that �Please type here your question : question number: 1� is shown.
//		On �Please type here your question ; question number: 1� screen, type in textbox: �TEST QUESTION ONE� � with double question mark, and press on Next.
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 1� is shown.
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, type the following answers in this order: "ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE".
//		Make sure that 4 fields are filled with the answers typed.
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 1� screen, mark the radio button of the third answer, and press on Next.
//		Make sure that �Please type here your question : question number: 2� is shown.
//		On �Please type here your question ; question number: 2� screen, type in textbox: �TEST QUESTION TWO� � with double question mark, and press on Next.
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 2� is shown.
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO".
//		Make sure that 4 fields are filled with the answers typed.
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, mark the radio button of the third answer, and press on Next.
//		Make sure that �Please type here your question : question number: 3� is shown.
//		On �Please type here your question ; question number: 3� screen, type in textbox: �TEST QUESTION THREE� � with double question mark, and press on Next.
//		Make sure that �Please enter 4 possible answers and Mark the right one! ; question number: 3� is shown.
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 2� screen, type the following answers in this order: "ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE".
//		Make sure that 4 fields are filled with the answers typed.
//		On �Please enter 4 possible answers and Mark the right one! ; question number: 3� screen, mark the radio button of the third answer, and press on Next.
//		Make sure that �You finished to build the test � let�s play!!� screen is shown.
//		On �You finished to build the test � let�s play!!� screen, press the Play button.
//		Make sure that question  has only one question mark at the end of the title.
//		On Test screen, choose the third answer and press Next!
//		Make sure that question has only one question mark at the end of the title.
//		On Test screen, choose the third answer and press Next!
//		Make sure that question  has only one question mark at the end of the title.
//		On Test screen, choose the third answer and press Next!
//		Make sure that Test summary is shown, and green �Success� banner is shown.
//		On Test summary screen, press the Quit button
//		Make sure that Trivia web application is closed � white page is shown in browser.
//		Close web browser window
//		Make sure that web browser window is closed.
		
		if(failed) {
			fail("Test has failed. See messages in console.");
		}
		
	}
*/

}
