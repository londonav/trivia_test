import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.openqa.selenium.Alert;

@TestMethodOrder(OrderAnnotation.class)
class EmptyFieldQuestionTitle extends setupTest{
	//TC02
	@Test
	@Order(1)
	void emptyFieldQuestion1() {
		firstRun();
		setupBuildTest();
		setupFields();
		startButton.click();
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		nextButtonBuildTest.click();
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		removeFields();
		nextTest();
	}
	
	//TC03
	@Test
	@Order(2)
	void emptyFieldQuestion2() {
		setupBuildTest();
		setupFields();
		startButton.click();
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		nextButtonBuildTest.click();
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		nextButtonBuildTest.click();
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		removeFields();
		nextTest();
	}
	
	//TC04
	@Test
	@Order(3)
	void emptyFieldQuestion3() {
		setupBuildTest();
		setupFields();
		startButton.click();
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		nextButtonBuildTest.click();
		assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 0);
		nextButtonBuildTest.click();
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		nextButtonBuildTest.click();
		assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		removeFields();
		nextTest();
		
	}
	
	//TC06
	@Test
	@Order(4)
	void markRadioButtonEmptyFieldsAnswer() {
		setupBuildTest();
		setupFields();
		Alert alertMessage = null;
		startButton.click();
		boolean failed = false;
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());		
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());		
		nextButtonBuildTest.click();		
		alertMessage = driver.switchTo().alert();
		try {
			assertEquals("You have to fill all the fields please", alertMessage.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		alertMessage.accept();		
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		selectRadioButton(0);
		nextButtonBuildTest.click();
		try {
			assertEquals("You have to fill all the fields please", alertMessage.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		alertMessage.accept();		
		assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());		
		finalize();
		
		if(failed) {
			fail("Test has failed. See messages in console.");
		}

	}
	 
}
