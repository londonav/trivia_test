import org.junit.jupiter.api.Test;

class sanityTest extends setupTest{
	@Test
	void test() {
		firstRun();
		setupBuildTest();
		setupFields();
		startButton.click();
		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		nextButtonBuildTest.click();
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 1);
		nextButtonBuildTest.click();
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();
		playButton.click();
		setupTestElements();
		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();
		answersQtwoRadioButtonsTest.get(1).click();
		nextButtonTest.click();
		answersQthreeRadioButtonsTest.get(0).click();
		nextButtonTest.click();
		finalize();
	}

}
