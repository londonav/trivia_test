import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SpecialCharsTest extends setupTest{

	@Test
	//TC01
	void testSpecialCharacters() {
//		On Trivia logo screen, press Start.
		firstRun();
		setupBuildTest();
		startButton.click();
//		Make sure that �Please type here your question : question number: 1� screen is shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
//		Enter special characters and numbers in "Please type here your question : question number: 1"
		EnterQuestion("!@#$%%^&*()_+1234567890-=");
		nextButtonBuildTest.click();
//		Make sure that �Please type here your question : question number: 1� screen is still shown.
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
//		Close web browser window
//		Make sure that web browser window is closed.
		finalize();
	}

}
