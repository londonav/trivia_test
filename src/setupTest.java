import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

class setupTest {
	
	static WebDriver driver = new ChromeDriver();
	static WebElement startButton = null;
	static WebElement questionField = null;
	static WebElement nextButtonBuildTest = null;
	static WebElement backButtonBuildTest = null;
	static WebElement playButton = null;
	static WebElement quitButtonBuiltTest = null;
	static WebElement quitButtonEndGame = null;
	static WebElement nextButtonTest = null;
	static WebElement backButtonTest = null;
	static ArrayList<WebElement> answersBuildTest = null;
	static ArrayList<WebElement> answersRadioButtonsBuildTest = null;
	static ArrayList<WebElement> answersQoneRadioButtonsTest = null;
	static ArrayList<WebElement> answersQtwoRadioButtonsTest = null;
	static ArrayList<WebElement> answersQthreeRadioButtonsTest = null;
	static WebElement enterQuestionText = null;
	static WebElement enterAnswerText = null;
	static WebElement readyToTest = null;
	static WebElement testResult = null;
	static WebElement tryAgainButtonTest = null;
	static WebElement QuitButtonTest = null;
	static WebElement QoneTitle = null;
	static WebElement QtwoTitle = null;
	static WebElement QthreeTitle = null;
	static WebElement triviaLogo = null;
	
	
	public static void AddAnswerElements(List<String> elements, ArrayList<WebElement> listElements) {
		for (String value : elements) {
			listElements.add(driver.findElement(By.cssSelector(value)));
        }
	}
	
	public static void EnterQuestion(String question) {
		questionField.click();
		questionField.sendKeys(question);
	}
	
	public static void EnterAnswerAndSelectRadioButton(String answerOne, String answerTwo, String answerThree, String answerFour, int radioButton) {
		answersBuildTest.get(0).click();
		answersBuildTest.get(0).sendKeys(answerOne);
		answersBuildTest.get(1).click();
		answersBuildTest.get(1).sendKeys(answerTwo);
		answersBuildTest.get(2).click();
		answersBuildTest.get(2).sendKeys(answerThree);
		answersBuildTest.get(3).click();
		answersBuildTest.get(3).sendKeys(answerFour);
		answersRadioButtonsBuildTest.get(radioButton).click();
	}
	
	public static void selectRadioButton(int radioButton) {
		answersRadioButtonsBuildTest.get(radioButton).click();
	}
	
	
	@BeforeTest
	public static void firstRun() {
		driver.manage().window().maximize();
		driver.get("https://shemsvcollege.github.io/Trivia");
	}
	
	@BeforeTest
	public static void setupBuildTest() {
			//Answers fields
			//  #answers > div:nth-child(2) > div.col-sm-11 > input
			//  #answers > div:nth-child(3) > div.col-sm-11 > input
			//  #answers > div:nth-child(4) > div.col-sm-11 > input
			//  #answers > div:nth-child(5) > div.col-sm-11 > input
		
		//Answers radio buttons
		//  #answers > div:nth-child(2) > div.col-sm-1 > input[type=radio]
		//  #answers > div:nth-child(3) > div.col-sm-1 > input[type=radio]
		//  #answers > div:nth-child(4) > div.col-sm-1 > input[type=radio]
		//  #answers > div:nth-child(5) > div.col-sm-1 > input[type=radio]
		
			//Quit and Next buttons
			//  #secondepage > center > button:nth-child(2)
			//  #secondepage > center > button:nth-child(3)
		
		try {
			startButton = driver.findElement(By.id("startB"));
			questionField = driver.findElement(By.name("question"));
			nextButtonBuildTest = driver.findElement(By.id("nextquest"));
			backButtonBuildTest = driver.findElement(By.id("backquest"));
			playButton = driver.findElement(By.cssSelector("#secondepage > center > button:nth-child(2)"));
			quitButtonBuiltTest = driver.findElement(By.cssSelector("#secondepage > center > button:nth-child(3)"));
			enterQuestionText = driver.findElement(By.xpath("/html/body/div/section[1]/main/div/div[2]/form/div/legend"));
			enterAnswerText = driver.findElement(By.xpath("/html/body/div/section[1]/main/div/form/div/div/legend"));
			readyToTest = driver.findElement(By.id("needBackGround"));
			triviaLogo = driver.findElement(By.xpath("/html/body/div/header/img"));
			
			
			
			
			
//			answersRadioButtons.add(driver.findElement(By.cssSelector("#answers > div:nth-child(2) > div.col-sm-1 > input[type=radio]")));
//			answersRadioButtons.add(driver.findElement(By.cssSelector("#answers > div:nth-child(3) > div.col-sm-1 > input[type=radio]")));
//			answersRadioButtons.add(driver.findElement(By.cssSelector("#answers > div:nth-child(4) > div.col-sm-1 > input[type=radio]")));
//			answersRadioButtons.add(driver.findElement(By.cssSelector("#answers > div:nth-child(5) > div.col-sm-1 > input[type=radio]")));
			
			// Test answer radio buttons
			//  #\\32  > input:nth-child(3)
			//  #\\32  > input:nth-child(6)
			//  #\\32  > input:nth-child(9)
			//  #\\32  > input:nth-child(12)
			
			//  #\\31  > input:nth-child(3)
			//  #\\31  > input:nth-child(6)
			//  #\\31  > input:nth-child(9)
			//  #\\31  > input:nth-child(12)
			
			
			//  #\\30  > input:nth-child(3)
			//  #\\30  > input:nth-child(6)
			//  #\\30  > input:nth-child(9)
			//  #\\30  > input:nth-child(12)
			
		}
		catch(Exception e) {
			System.out.println("Cause is: " + e.getCause());
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void setupFields() {
		try {
			answersBuildTest = new ArrayList<WebElement>();
			answersRadioButtonsBuildTest = new ArrayList<WebElement>();
			List<String> answersElements = Arrays.asList("#answers > div:nth-child(2) > div.col-sm-11 > input", "#answers > div:nth-child(3) > div.col-sm-11 > input", "#answers > div:nth-child(4) > div.col-sm-11 > input", "#answers > div:nth-child(5) > div.col-sm-11 > input");
			AddAnswerElements(answersElements, answersBuildTest);
			List<String> answersRadioElementsBuildTest = Arrays.asList("#answers > div:nth-child(2) > div.col-sm-1 > input[type=radio]", "#answers > div:nth-child(3) > div.col-sm-1 > input[type=radio]", "#answers > div:nth-child(4) > div.col-sm-1 > input[type=radio]", "#answers > div:nth-child(5) > div.col-sm-1 > input[type=radio]");
			AddAnswerElements(answersRadioElementsBuildTest, answersRadioButtonsBuildTest);
		}
		catch(Exception e) {
			System.out.println("Cause is: " + e.getCause());
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	@BeforeTest
	public static void setupTestElements() {
		try {
			answersQoneRadioButtonsTest = new ArrayList<WebElement>();
			answersQtwoRadioButtonsTest = new ArrayList<WebElement>();
			answersQthreeRadioButtonsTest = new ArrayList<WebElement>();
			nextButtonTest = driver.findElement(By.id("btnnext"));
			backButtonTest = driver.findElement(By.id("btnback"));
			testResult = driver.findElement(By.id("mark"));
			tryAgainButtonTest = driver.findElement(By.xpath("/html/body/div/section[4]/center/button[1]"));
			QuitButtonTest = driver.findElement(By.xpath("/html/body/div/section[4]/center/button[2]"));
			List<String> answersRadioElementsTest = Arrays.asList("#\\32  > input:nth-child(3)", "#\\32  > input:nth-child(6)", "#\\32  > input:nth-child(9)", "#\\32  > input:nth-child(12)");
			AddAnswerElements(answersRadioElementsTest, answersQoneRadioButtonsTest);
			answersRadioElementsTest = null;
			answersRadioElementsTest = Arrays.asList("#\\31  > input:nth-child(3)", "#\\31  > input:nth-child(6)", "#\\31  > input:nth-child(9)", "#\\31  > input:nth-child(12)");
			AddAnswerElements(answersRadioElementsTest, answersQtwoRadioButtonsTest);
			answersRadioElementsTest = null;
			answersRadioElementsTest = Arrays.asList("#\\30  > input:nth-child(3)", "#\\30  > input:nth-child(6)", "#\\30  > input:nth-child(9)", "#\\30  > input:nth-child(12)");
			AddAnswerElements(answersRadioElementsTest, answersQthreeRadioButtonsTest);
			QoneTitle = driver.findElement(By.xpath("/html/body/div/section[3]/div[3]/h3"));
			QtwoTitle = driver.findElement(By.xpath("/html/body/div/section[3]/div[2]/h3"));
			QthreeTitle = driver.findElement(By.xpath("/html/body/div/section[3]/div[1]/h3"));
			
		}
		
		catch(Exception e) {
			System.out.println("Cause is: " + e.getCause());
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	@AfterTest
	public static void nextTest() {
		driver.get("https://shemsvcollege.github.io/Trivia");
		Alert alert = driver.switchTo().alert();
	    alert.accept();
	}
	
	@AfterTest
	public static void removeFields() {
		answersBuildTest = null;
		answersRadioButtonsBuildTest = null;
		answersQoneRadioButtonsTest = null;
		answersQtwoRadioButtonsTest = null;
		answersQthreeRadioButtonsTest = null;
		
		answersBuildTest = new ArrayList<WebElement>();
		answersRadioButtonsBuildTest = new ArrayList<WebElement>();
		answersQoneRadioButtonsTest = new ArrayList<WebElement>();
		answersQtwoRadioButtonsTest = new ArrayList<WebElement>();
		answersQthreeRadioButtonsTest = new ArrayList<WebElement>();
	}
	
	@AfterTest
	public void finalize() {
		driver.quit();
	}

}
