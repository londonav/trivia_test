import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebElement;

@TestMethodOrder(OrderAnnotation.class)
class ContentAndBehaviorApp extends setupTest {

	public boolean isAlertPresent() 
	{ 
	    try 
	    { 
	        driver.switchTo().alert(); 
	        return true; 
	    }  
	    catch (NoAlertPresentException Ex) 
	    { 
	        return false; 
	    }   
	} 
	
	//TC16
	@Disabled
	@Test
	@Order(1)
	void checkNextAndBackButtons() {
		firstRun();
		setupBuildTest();
		setupFields();
	
		startButton.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}

		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}
		

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}

		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 0);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}
		
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();
		
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}
		
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 0);
		backButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}
		
		backButtonBuildTest.click();
		
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}

		backButtonBuildTest.click();
		
		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}
		
		
		backButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}
		

		backButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			nextTest();
			fail("Message is: " + e.getMessage());
		}
		
		nextTest();
	}
	
	//TC17
	@Disabled
	@Test
	@Order(2)
	void checkWritingTypos(){
//		Do pre-test condition.
		setupBuildTest();
		setupFields();
		
		boolean failed = false;
		String messageIntro = driver.findElement(By.xpath("/html/body/div/section[1]/main/div/div[1]/legend")).getText();
		String placeholdersActual = null;
		String placeholdersExpected = null;
		Alert alertMessage = null;
		String Qone = null;
		String Qtwo = null;
		String Qthree = null;
		
		try {
			assertEquals("QA Test. To start the test: Push the button below. Good luck!", messageIntro);
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}
		startButton.click();
		
		try {
			assertEquals("Please type here your question :\nQuestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and mark the right one!\nQuestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}
		
		placeholdersActual = answersBuildTest.get(0).getAttribute("placeholder") + "|" + answersBuildTest.get(1).getAttribute("placeholder") + "|" + answersBuildTest.get(2).getAttribute("placeholder") + "|" + answersBuildTest.get(3).getAttribute("placeholder");
		placeholdersExpected = "First answer|Second answer|Third answer|Fourth answer";
		try {
			assertEquals(placeholdersExpected, placeholdersActual);
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}
		
		nextButtonBuildTest.click();

		alertMessage = driver.switchTo().alert();
		try {
			assertEquals("You have to fill all the fields please", alertMessage.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}

		alertMessage.accept();

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		nextButtonBuildTest.click();
		
		try {
			assertEquals("Please type here your question :\nQuestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();
	
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nQuestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}

		placeholdersActual = answersBuildTest.get(0).getAttribute("placeholder") + "|" + answersBuildTest.get(1).getAttribute("placeholder") + "|" + answersBuildTest.get(2).getAttribute("placeholder") + "|" + answersBuildTest.get(3).getAttribute("placeholder");
		placeholdersExpected = "First answer|Second answer|Third answer|Fourth answer";
		try {
			assertEquals(placeholdersExpected, placeholdersActual);
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 0);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nQuestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nQuestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}

		placeholdersActual = answersBuildTest.get(0).getAttribute("placeholder") + "|" + answersBuildTest.get(1).getAttribute("placeholder") + "|" + answersBuildTest.get(2).getAttribute("placeholder") + "|" + answersBuildTest.get(3).getAttribute("placeholder");
		placeholdersExpected = "First answer|Second answer|Third answer|Fourth answer";
		try {
			assertEquals(placeholdersExpected, placeholdersActual);
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 0);
		nextButtonBuildTest.click();

		try {
			assertEquals("You finished to build the test � Let�s play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
//			e.printStackTrace();
			failed = true;
		}
		
		playButton.click();
		setupTestElements();

		try {
				assertEquals(Qthree, QoneTitle.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
		}
			
		nextButtonTest.click();

		if(isAlertPresent()) {
			alertMessage = driver.switchTo().alert();
			try {
				assertEquals("You have to mark an answer!!", alertMessage.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}
		}
		else {
			try {
			assertEquals("You have to mark an answer!!", alertMessage.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}
			try {
				assertEquals(Qthree, QoneTitle.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}	
		}

		alertMessage.accept();
		alertMessage = null;

		try {
				assertEquals(Qthree, QoneTitle.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}
		
		answersQoneRadioButtonsTest.get(0).click();
		nextButtonTest.click();

		try {
				assertEquals(Qtwo, QtwoTitle.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}

		answersQtwoRadioButtonsTest.get(0).click();
		nextButtonTest.click();

		try {
				assertEquals(Qone, QthreeTitle.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}

		answersQthreeRadioButtonsTest.get(0).click();
		nextButtonTest.click();

		try {
				assertEquals("Success", testResult.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}
		
		tryAgainButtonTest.click();
		setupTestElements();

		try {
				assertEquals(Qthree, QoneTitle.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}

		answersQoneRadioButtonsTest.get(1).click();
		nextButtonTest.click();

		try {
				assertEquals(Qtwo, QtwoTitle.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}

		answersQtwoRadioButtonsTest.get(1).click();
		nextButtonTest.click();

		try {
				assertEquals(Qone, QthreeTitle.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}

		answersQthreeRadioButtonsTest.get(1).click();
		nextButtonTest.click();

		try {
				assertEquals("Failed", testResult.getText());
			} catch (AssertionError e) {
				System.out.println("Message is: " + e.getMessage());
//				e.printStackTrace();
				failed = true;
			}

		QuitButtonTest.click();

		nextTest();
		
		if(failed) {
			fail("Test has failed. See messages in console.");
		}
		
		
	}
	
	//TC18
	@Disabled
	@Test
	@Order(3)
	void closeAppInBuildAndTest() {
		firstRun();
		setupBuildTest();
		setupFields();
		
		boolean failed = false;

		startButton.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();


		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		
		quitButtonBuiltTest.click();

		if(triviaLogo.isDisplayed()) {
			System.out.println("Trivia web application was not closed");
			nextTest();
//			fail("Trivia web application was not closed");
			failed = true;
			setupBuildTest();
			setupFields();
			
		}
		else{
			driver.get("https://shemsvcollege.github.io/Trivia");
		}
		

		startButton.click();
		
		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();
		
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		playButton.click();
		setupTestElements();

		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("Success", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		QuitButtonTest.click();

		finalize();

		if(failed) {
			fail("Test has failed. See messages in console.");
		}



	}
	
	//TC19
	@Disabled
	@Test
	@Order(4)
	void checkAnswerFieldsWhenUseBackButton() {
		firstRun();
		setupBuildTest();
		setupFields();
		
		String testQuestionField = null;
		String textBoxesActual = null;
		ArrayList<Boolean> radioButtonsTest = null;
		radioButtonsTest = new ArrayList<Boolean>();

		startButton.click();
	
		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}

		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		backButtonBuildTest.click();

		testQuestionField = questionField.getAttribute("value");
		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
			assertEquals("TEST QUESTION ONE", testQuestionField);
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}
		
		nextButtonBuildTest.click();

		textBoxesActual = answersBuildTest.get(0).getAttribute("value") + "|" + answersBuildTest.get(1).getAttribute("value") + "|" + answersBuildTest.get(2).getAttribute("value") + "|" + answersBuildTest.get(3).getAttribute("value");
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(0).isSelected());
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(1).isSelected());
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(2).isSelected());
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(3).isSelected());
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
			assertEquals("|||", textBoxesActual);
			assertEquals(false, radioButtonsTest.get(0));
			assertEquals(false, radioButtonsTest.get(1));
			assertEquals(false, radioButtonsTest.get(2));
			assertEquals(false, radioButtonsTest.get(3));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}
		radioButtonsTest = null;
		radioButtonsTest = new ArrayList<Boolean>();
				
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 0);
		nextButtonBuildTest.click();
		
		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}
		
		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();
		
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
		}
		
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 0);
		backButtonBuildTest.click();
		
		testQuestionField = questionField.getAttribute("value");
		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
			assertEquals("TEST QUESTION TWO", testQuestionField);
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}
		
		nextButtonBuildTest.click();
		
		textBoxesActual = answersBuildTest.get(0).getAttribute("value") + "|" + answersBuildTest.get(1).getAttribute("value") + "|" + answersBuildTest.get(2).getAttribute("value") + "|" + answersBuildTest.get(3).getAttribute("value");
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(0).isSelected());
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(1).isSelected());
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(2).isSelected());
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(3).isSelected());
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
			assertEquals("|||", textBoxesActual);
			assertEquals(false, radioButtonsTest.get(0));
			assertEquals(false, radioButtonsTest.get(1));
			assertEquals(false, radioButtonsTest.get(2));
			assertEquals(false, radioButtonsTest.get(3));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}		
		radioButtonsTest = null;
		radioButtonsTest = new ArrayList<Boolean>();
		
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 0);
		nextButtonBuildTest.click();
		
		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}
		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();
		
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}
		
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 0);
		backButtonBuildTest.click();
		
		testQuestionField = questionField.getAttribute("value");
		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
			assertEquals("TEST QUESTION THREE", testQuestionField);
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}
		
		nextButtonBuildTest.click();
		
		textBoxesActual = answersBuildTest.get(0).getAttribute("value") + "|" + answersBuildTest.get(1).getAttribute("value") + "|" + answersBuildTest.get(2).getAttribute("value") + "|" + answersBuildTest.get(3).getAttribute("value");
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(0).isSelected());
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(1).isSelected());
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(2).isSelected());
		radioButtonsTest.add(answersRadioButtonsBuildTest.get(3).isSelected());
		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
			assertEquals("|||", textBoxesActual);
			assertEquals(false, radioButtonsTest.get(0));
			assertEquals(false, radioButtonsTest.get(1));
			assertEquals(false, radioButtonsTest.get(2));
			assertEquals(false, radioButtonsTest.get(3));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			fail("Message is: " + e.getMessage());
		}		
		radioButtonsTest = null;
		
		finalize();
	}
	
	//TC20
	@Disabled
	@Test
	@Order(5)
	void checkQuestionsOrderTest () {
		firstRun();
		setupBuildTest();
		setupFields();

		boolean failed = false;
		
		startButton.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterQuestion("TEST QUESTION TWO");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}


		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();


		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		
		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		
		playButton.click();
		setupTestElements();

		try {
			assertNotEquals("TEST QUESTION ONE", QoneTitle.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}

		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();
		
		try {
			assertNotEquals("TEST QUESTION TWO", QtwoTitle.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		
		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();
		
		try {
			assertNotEquals("TEST QUESTION THREE", QthreeTitle.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			e.printStackTrace();
			failed = true;
		}
		
		finalize();
		if(failed) {
			fail("Test has failed. See messages in console.");
		}
		
	}
/*
	//TC21
	@Test
	@Order(6)
	void checkSingleQuestionMark () {
		firstRun();
		setupBuildTest();
		setupFields();

		boolean failed = false;

		startButton.click();
		
		try {
			assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}


		EnterQuestion("TEST QUESTION ONE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

			EnterQuestion("TEST QUESTION TWO");
			nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterQuestion("TEST QUESTION THREE");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		playButton.click();
		setupTestElements();

		try {
			assertEquals("?", QoneTitle.getText().substring(QoneTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("?", QtwoTitle.getText().substring(QtwoTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("?", QthreeTitle.getText().substring(QthreeTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}


		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("Sucsses", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		QuitButtonTest.click();
		removeFields();
		driver.get("https://shemsvcollege.github.io/Trivia");
		setupBuildTest();
		setupFields();

		startButton.click();
		
		assertEquals("Please type here your question :\nquestion number: 1", enterQuestionText.getText());

		EnterQuestion("TEST QUESTION ONE?"); 
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 1", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION ONE", "ANSWER TWO QUESTION ONE", "ANSWER THREE QUESTION ONE", "ANSWER FOUR QUESTION ONE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 2", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterQuestion("TEST QUESTION TWO?");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 2", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION TWO", "ANSWER TWO QUESTION TWO", "ANSWER THREE QUESTION TWO", "ANSWER FOUR QUESTION TWO", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("Please type here your question :\nquestion number: 3", enterQuestionText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterQuestion("TEST QUESTION THREE?");
		nextButtonBuildTest.click();

		try {
			assertEquals("Please enter 4 possible answers and Mark the right one!\nquestion number: 3", enterAnswerText.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		EnterAnswerAndSelectRadioButton("ANSWER ONE QUESTION THREE", "ANSWER TWO QUESTION THREE", "ANSWER THREE QUESTION THREE", "ANSWER FOUR QUESTION THREE", 2);
		nextButtonBuildTest.click();

		try {
			assertEquals("You finished to build the test - lets play!!", readyToTest.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		playButton.click();
		setupTestElements();

		try {
			assertEquals("?", QoneTitle.getText().substring(QoneTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQoneRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("?", QtwoTitle.getText().substring(QtwoTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQtwoRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("?", QthreeTitle.getText().substring(QthreeTitle.getText().length() - 1));
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		answersQthreeRadioButtonsTest.get(2).click();
		nextButtonTest.click();

		try {
			assertEquals("Sucsses", testResult.getText());
		} catch (AssertionError e) {
			System.out.println("Message is: " + e.getMessage());
			
			failed = true;
		}

		QuitButtonTest.click();
		removeFields();
		
		finalize();
		if(failed) {
			fail("Test has failed. See messages in console.");
		}
	} 
	*/
	
}
